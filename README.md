# Assignment For Mobile Engineer



## Task

In this assignment, you are required to develop a weather application using Flutter that retrieves and displays weather data from the OpenWeatherMap API. The application should include the following features:

- Display Weather Information: The application should display the weather data for the user's current location and retrieved from the API, including the current temperature, humidity, wind speed, and a forecast for the next 5 days.
- Error Handling: The application should handle any errors that occur during the data retrieval process and display appropriate error messages to the user.


## Requirements

- Use Flutter to develop the mobile application.
- Use the OpenWeatherMap API to retrieve weather data. (https://openweathermap.org/api)
- Use a state management technique such as Cubit or BLoC to manage the state of the application.
- Write clean, readable, and well-documented code that follows best practices and adheres to the Flutter style guide.
- Submit your code on GitHub, GitLab or Bitbucket along with a README file that provides brief instructions on how to build and run the application.


## Grading Criteria

- Completion of all required features
- Quality and functionality of the user interface
- Implementation of state management technique
- Code quality, readability, and documentation
- Use of appropriate Flutter widgets
- Error handling
